package com.avenuecode.controller;

import com.avenuecode.domain.Order;
import com.avenuecode.service.OrderService;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test list all orders and order by Id.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

	@MockBean
	private OrderService orderService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getProducts() throws Exception {
		
		List<Order> mockOrderList = new ArrayList<>();
		Order mockOrder = new Order("Myorder1");
		mockOrder.setOrderedDate(Timestamp.valueOf(LocalDateTime.of(2017, 6, 27, 0, 0, 0)));
		mockOrder.setDescription("I ordered.");
		mockOrder.setInformation("Irvine");
		mockOrderList.add(mockOrder);

		given(orderService.read(1)).willReturn(mockOrder);
		System.out.println(orderService.read(1));
		System.out.println(mockOrder);
		
		mockOrder = new Order("Youorder1");
		mockOrder.setOrderedDate(Timestamp.valueOf(LocalDateTime.of(2017, 6, 27, 0, 0, 0)));
		mockOrder.setDescription("You ordered.");
		mockOrder.setInformation("Los Ang");
		mockOrderList.add(mockOrder);
        
		given(orderService.read(2)).willReturn(mockOrder);
		given(orderService.create(mockOrder)).willReturn(mockOrder);

		given(orderService.list()).willReturn(mockOrderList);

		this.mockMvc.perform(get("/orders")).andExpect(status().isOk())
				.andExpect(content().string(containsString("Irvine")));

	}
}
package com.avenuecode.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.avenuecode.domain.Product;

@Repository
@RepositoryRestResource(collectionResourceRel = "product", path = "product")
public interface ProductsRepository extends CrudRepository<Product, Long> {
	
	    @Override
	    @RestResource(exported = false)
	    void deleteAll();
}

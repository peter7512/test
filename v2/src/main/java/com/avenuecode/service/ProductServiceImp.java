package com.avenuecode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.domain.Product;
import com.avenuecode.repository.ProductsRepository;

@Service
public class ProductServiceImp implements ProductService {

	private ProductsRepository productsRepository;

	@Autowired
	public ProductServiceImp(ProductsRepository productsRepository) {
		this.productsRepository = productsRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.avenuecode.service.ProductService#list()
	 */
	@Override
	public Iterable<Product> list() {

		return productsRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.avenuecode.service.ProductService#read(long)
	 */
	@Override
	public Product read(long id) {

		return productsRepository.findOne(id);
	}

}

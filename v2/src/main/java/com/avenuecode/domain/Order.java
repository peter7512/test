package com.avenuecode.domain;

import com.avenuecode.tools.*;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

@Entity
@Table(name = "ORDER1")
@Data
public class Order {
	@Id
	@GeneratedValue
	// @Column(name="ORDER_ID")
	private Long orderId;

	@OneToMany(mappedBy = "orderId")
	List<OrderProduct> orderProducts;

	private String name;
	private String description;

	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderedDate;

	private String information;

	private Order() {
		// orderProducts=new ArrayList<>();
	}

	public Order(String name) {
		this.name = name;
		// orderProducts=new ArrayList<>();
	}

	public List<OrderProduct> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProduct> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getOrderedDate() {
		return orderedDate;
	}
	
	 @JsonDeserialize(using = JsonDateDeserializer.class)
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((information == null) ? 0 : information.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((orderProducts == null) ? 0 : orderProducts.hashCode());
		result = prime * result + ((orderedDate == null) ? 0 : orderedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (information == null) {
			if (other.information != null)
				return false;
		} else if (!information.equals(other.information))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (orderProducts == null) {
			if (other.orderProducts != null)
				return false;
		} else if (!orderProducts.equals(other.orderProducts))
			return false;
		if (orderedDate == null) {
			if (other.orderedDate != null)
				return false;
		} else if (!orderedDate.equals(other.orderedDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderProducts=" + orderProducts + ", name=" + name + ", description="
				+ description + ", orderedDate=" + orderedDate + ", information=" + information + "]";
	}

}

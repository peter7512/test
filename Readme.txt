﻿I used spring boot 1.5.4. Java 8 and Maven 3.3.9, I wrote two methods to finish the task. One method is through Spring data rest directly; 
 The other is through writing RestController API by myself.  and I wrote automated tests for getting information, 
 and also wrote  a client program to simulate the clients to use it for placing and updating automated test . 
 I  deployed on aws ec2:(ec2-34-208-115-156.us-west-2.compute.amazonaws.com.)  It is running.
 Three version source code(1.src  2.v2  3. v3-pws) all work fine.
 Codes in v3-pws directory are the last version that deployed on Pivotal Web services: https://lizhihuaapp.cfapps.io/orders  (no port)  It is running.
 


If using version v2 source code for manual tests,change port 8080 to 9000. both version are ok.
The HAL Browser (for Spring Data REST) click link:http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/api 
                                               or http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:9000/api

****To compile and run the application.
1.Download source code and three ways to run the application (In source code  root directory  command Line . I used Java version: 1.8.0_111 and Maven 3.3.9.)  
                           I. mvn spring-boot:run  (compile and run by using spring-boot maven plugin)
                           II. run script.sh file directly;   script.sh 
                           III. Execute the content separatly .  a. mvn  package                  b.  java -jar target/code-1.0.jar


2. I had deployed on my aws account. It is running on ec2-34-208-115-156.us-west-2.compute.amazonaws.com.
3. I had deployed on Pivotal Web services: https://lizhihuaapp.cfapps.io/orders .

****To run the suite of automated tests.
(In source code  root directory  command Line . I used Java version: 1.8.0_111 and Maven 3.3.9.)  
Execute: mvn  test   or  mvn package.
I wrote a client program to simulate the clients to use it for placing and updating automated test . 


****If you do not install java 8 or maven, To test remote running on aws manually with Rest client   or just click the links:
              If using version v2 source code for manual tests,change port 8080 to 9000. both version are ok.
1. For Products Test  implement two methods:
        a.list product catalog
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/products
(This is the result using spring restController)
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/api/product
(This is the result I use spring data rest directly)
       b.read a single product by its id
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/products/1
(This is the result using spring restController)
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/api/product/1
(This is the result I use spring data rest directly)


  II. For Orders test implement four methods:
    a.place an order
            http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders
            Order Body Example(in the last of this file)
    b.modify an existing order
           http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders/1
            Order Body Example(in the last of this file)
    c.list placed orders
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders
(This is the result using spring restController)
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/api/orders
(This is the result I use spring data rest directly)
     d.read an existing order by its id
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders/1
(This is the result using spring restController)
http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/api/orders/1
(This is the result I use spring data rest directly)
      
**** Order  Body Example:
{
    "orderId": 1,
    "orderProducts": [
        {
            "orderProductId": 1,
            "orderId": 1,
            "product": {
                "productId": 1,
                "name": "Pen",
                "description": "It is a pen.",
                "price": 9.55,
                "information": "Other info of pen"
            },
            "amount": 4,
            "info": "I ordered pens."
        },
        {
            "orderProductId": 2,
            "orderId": 1,
            "product": {
                "productId": 2,
                "name": "Paper",
                "description": "It is a paper.",
                "price": 1.55,
                "information": "other info of paper"
            },
            "amount": 41,
            "info": "I ordered papers"
        }
    ],
    "name": "Myorder1",
    "description": "I ordered.",
    "orderedDate": "06/27/2017 12:00:00 AM",
    "information": "Irvine"
}
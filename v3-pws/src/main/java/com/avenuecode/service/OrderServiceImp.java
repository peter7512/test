package com.avenuecode.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.domain.Order;

import com.avenuecode.repository.OrderProductRepository;
import com.avenuecode.repository.OrdersRepository;

@Service
public class OrderServiceImp implements OrderService {

	private OrdersRepository ordersRepository;
	private OrderProductRepository orderProductRepository;

	@Autowired
	public OrderServiceImp(OrdersRepository ordersRepository, OrderProductRepository orderProductRepository) {
		this.ordersRepository = ordersRepository;
		this.orderProductRepository = orderProductRepository;
	}

	
	@Override
	public Iterable<Order> list() {

		return ordersRepository.findAll();
	}

	
	@Override
	public Order read(long id) {

		return ordersRepository.findOne(id);
	}

	
	@Override
	@Transactional
	public Order create(Order order) {
		order.setOrderId(null);
		order.setOrderedDate(new Date());
		Order o = ordersRepository.save(order);
		order.getOrderProducts().forEach(x -> {
			x.setOrderId(o.getOrderId());
			x.setOrderProductId(null);
		});
		order.setOrderId(o.getOrderId());
		orderProductRepository.save(order.getOrderProducts());
		//return order;
		return o;
		
	}

	
	@Override
	public Order update(long id, Order update) {
		Order order = ordersRepository.findOne(id);

		orderProductRepository.delete(order.getOrderProducts());
	
		update.getOrderProducts().forEach(x -> {
			x.setOrderId(id);
			x.setOrderProductId(null);
		});
		orderProductRepository.save(update.getOrderProducts());
				
		if (update.getName() != null)
			order.setName(update.getName());
		order.setOrderProducts(update.getOrderProducts());
		order.setInformation(update.getInformation());
		order.setDescription(update.getDescription());
		order.setOrderedDate(new Date());
		return ordersRepository.save(order);		
	
	}

	public void delete(long id) {

		ordersRepository.delete(id);

	}

}

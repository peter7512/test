package com.avenuecode.service;


import com.avenuecode.domain.Order;

public interface OrderService {

	Iterable<Order> list();

	Order read(long id);

	Order create(Order order);

	Order update(long id, Order update);

}
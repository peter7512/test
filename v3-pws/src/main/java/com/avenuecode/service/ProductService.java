package com.avenuecode.service;

import com.avenuecode.domain.Product;

public interface ProductService {

	Iterable<Product> list();

	Product read(long id);

}
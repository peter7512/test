package com.avenuecode.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Entity
@Table(name = "ORDER_PRODUCT")
@Data
public class OrderProduct {
	@Id
	@GeneratedValue
	@Column(name = "ORDER_PRODUCT_ID")
	@JsonIgnore
	private Long orderProductId;

	@JsonIgnore
	private Long orderId;
	
	@ManyToOne
	private Product product;

	private Integer amount;

	private String info;

	private OrderProduct() {

	}

	
}

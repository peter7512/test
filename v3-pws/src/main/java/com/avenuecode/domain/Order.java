package com.avenuecode.domain;

import com.avenuecode.tools.*;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

@Entity
@Table(name = "ORDER1")
@Data
public class Order {
	@Id
	@GeneratedValue
	private Long orderId;
    
	private String name;
	private String description;
	
	@JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderedDate;

	private String information;

	@OneToMany(mappedBy = "orderId")
	List<OrderProduct> orderProducts;
	

	private Order() {
		
	}

	public Order(String name) {
		this.name = name;
		
	}

	
}

package com.avenuecode.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class Product {
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Long productId;

    @NotNull
	private String name;

	private String description;

	@DecimalMin("0.00")
	private Double price;

	private String information;

	private Product() {

	}

	public Product(String name) {

		this.name = name;
	}

	
}

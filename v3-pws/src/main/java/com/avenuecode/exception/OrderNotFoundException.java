package com.avenuecode.exception;

public class OrderNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2222222222222222222L;

	public OrderNotFoundException(String msg){
		super(msg);
	}
	
}
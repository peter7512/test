package com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.domain.Order;

@Repository
public interface OrdersRepository extends JpaRepository<Order, Long>{

}

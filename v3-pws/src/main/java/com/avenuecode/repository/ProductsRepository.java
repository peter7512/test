package com.avenuecode.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.avenuecode.domain.Product;

@Repository
@RepositoryRestResource(collectionResourceRel = "product", path = "products")
public interface ProductsRepository extends JpaRepository<Product, Long> {
	
	    @Override
	    @RestResource(exported = false)
	    void deleteAll();
}

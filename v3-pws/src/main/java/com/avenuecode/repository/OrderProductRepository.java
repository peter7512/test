package com.avenuecode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.avenuecode.domain.OrderProduct;

@RepositoryRestResource(exported = false)
public interface OrderProductRepository extends JpaRepository<OrderProduct, Long> {


}

package com.avenuecode.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.domain.Product;
import com.avenuecode.exception.OrderNotFoundException;
import com.avenuecode.service.ProductService;

/**
 * list products and read an existing order by its id.
 */
@RestController
@RequestMapping("/products")
public class ProductController {

	ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * list all products.
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public Iterable<Product> list() {
		return productService.list();
	}

	/**
	 * list an existing order by its id.
	 */
	@RequestMapping("/{productId}")
	public Product read(@PathVariable(value = "productId") long productId) {
		Product product = productService.read(productId);

		if (product == null) {
			throw new OrderNotFoundException("Product with id: " + productId + " not found.");
		}

		return product;
	}

	@ExceptionHandler(OrderNotFoundException.class)
	public void handlePostNotFound(OrderNotFoundException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}
}

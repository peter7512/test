package com.avenuecode.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.domain.Order;
import com.avenuecode.exception.OrderNotFoundException;
import com.avenuecode.service.OrderService;

/**
 * place an order modify an existing order list placed orders read an
 * existing order by its id
 */

@RestController
@RequestMapping("/orders")

public class OrderController {
	
	OrderService orderService;

	@Autowired
	public OrderController(OrderService orderService) {

		this.orderService = orderService;

	}

	/** list placed orders */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public Iterable<Order> list() {
		return orderService.list();
	}

	/** read an existing order by its id */
	@RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
	public Order read(@PathVariable(value = "orderId") long orderId) throws OrderNotFoundException {
		Order order = orderService.read(orderId);
		if (order == null) {
			throw new OrderNotFoundException("Order with id: " + orderId + " not found.");
		}
		return order;
	}

	/** place an order */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Order create(@RequestBody Order order) {
		return orderService.create(order);
	}

	/** modify an existing order */
	@RequestMapping(value = "/{orderId}", method = RequestMethod.PUT)
	public Order update(@PathVariable(value = "orderId") long orderId, @RequestBody Order order) {

		return orderService.update(orderId, order);
	}

	
	@ExceptionHandler(OrderNotFoundException.class)
	public void handleOrderNotFound(OrderNotFoundException exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

}

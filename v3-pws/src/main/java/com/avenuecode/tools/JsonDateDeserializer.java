package com.avenuecode.tools;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class JsonDateDeserializer extends JsonDeserializer<Date>  {
	
	 private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
				
		String dateAsString = jp.getText();
        try {
            if (dateAsString.isEmpty()) {
                return null;
            }
            else {
                return dateFormat.parse(dateAsString);
            }
        }
        catch (ParseException pe) {
            throw new RuntimeException(pe);
        }
	}

}

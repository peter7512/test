package com.avenuecode.controller;

import com.avenuecode.domain.Product;
import com.avenuecode.service.ProductService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.ArrayList;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Use two ways to test list all products and product by Id.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ProductController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class ProductControllerTest {

	// private MediaType contentType = new MediaType("application", "hal+json",
	// Charset.forName("UTF-8"));;
	private MediaType contentType = new MediaType("application", "json", Charset.forName("UTF-8"));

	@MockBean
	private ProductService productService;
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getProducts() throws Exception {

		List<Product> mockProductList = new ArrayList<>();
		Product mockProduct = new Product("Pen");
		mockProduct.setPrice(9.55);
		mockProduct.setDescription("It is a pen.");
		mockProduct.setInformation("Other info of pen");
		mockProductList.add(mockProduct);

		/**
		 * One way to test list products and product order by ID.
		 */
		given(productService.read(1)).willReturn(mockProduct);

		mockProduct = new Product("Paper");
		mockProduct.setPrice(1.55);
		mockProduct.setDescription("It is a paper.");
		mockProduct.setInformation("Other info of paper");
		mockProductList.add(mockProduct);
		given(productService.read(2)).willReturn(mockProduct);

		mockProduct = new Product("Box");
		mockProduct.setPrice(5.55);
		mockProduct.setDescription("It is a box.");
		mockProduct.setInformation("Other info of box");
		mockProductList.add(mockProduct);
		given(productService.read(3)).willReturn(mockProduct);

		given(productService.list()).willReturn(mockProductList);

		/**
		 * Another way to test list products and product order by ID and
		 * generate API document to target/snippets/products/ directory.
		 */
		mockMvc.perform(get("/products/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(content().string(containsString("Pen")))
				.andDo(document("/products/1"));
		mockMvc.perform(get("/products/2")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(content().string(containsString("Paper")))
				.andDo(document("/products/2"));

		mockMvc.perform(get("/products/3")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(content().string(containsString("Box")))
				.andDo(document("/products/3"));

		mockMvc.perform(get("/products")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Box"))).andExpect(content().contentType(contentType))
				.andDo(document("/products/all"));

		mockMvc.perform(get("/products/100")).andExpect(status().isNotFound()).andDo(document("/products/100"));
	}

	/**
	 * Test list product order by no existing ID and generate API document to
	 * target/snippets/products/100 directory.
	 */
	@Test
	public void productNotFound() throws Exception {
		mockMvc.perform(get("/products/100")).andExpect(status().isNotFound()).andDo(document("/products/100"));
	}

}
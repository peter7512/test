package com.avenuecode.controller;

import com.avenuecode.domain.Order;
import com.avenuecode.service.OrderService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Use two ways to test list all orders and list order by Id . Test create order
 * and update order.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(OrderController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class OrderControllerTest {

	@MockBean
	private OrderService orderService;

	@Autowired
	private MockMvc mockMvc;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private MediaType contentType = new MediaType("application", "json", Charset.forName("UTF-8"));

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Test
	public void getProducts() throws Exception {

		List<Order> mockOrderList = new ArrayList<>();
		Order mockOrder = new Order("Myorder1");
		mockOrder.setOrderedDate(Timestamp.valueOf(LocalDateTime.of(2017, 6, 27, 0, 0, 0)));
		mockOrder.setDescription("I ordered.");
		mockOrder.setInformation("Irvine");
		mockOrderList.add(mockOrder);

		/**
		 * One way to test list orders and list order by ID.
		 */
		given(orderService.read(1)).willReturn(mockOrder);
		System.out.println(orderService.read(1));
		System.out.println(mockOrder);

		mockOrder = new Order("Youorder1");
		mockOrder.setOrderedDate(Timestamp.valueOf(LocalDateTime.of(2017, 6, 27, 0, 0, 0)));
		mockOrder.setDescription("You ordered.");
		mockOrder.setInformation("Los Ang");
		mockOrderList.add(mockOrder);

		given(orderService.read(2)).willReturn(mockOrder);
		given(orderService.create(mockOrder)).willReturn(mockOrder);
		given(orderService.list()).willReturn(mockOrderList);

		/**
		 * Another way to test list orders and list order by ID and generate API
		 * document to target/snippets/orders/ directory.
		 */
		mockMvc.perform(get("/orders/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(content().string(containsString("Myorder1")))
				.andDo(document("/orders/1"));
		mockMvc.perform(get("/orders/2")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(content().string(containsString("Youorder1")))
				.andDo(document("/orders/2"));

		mockMvc.perform(get("/orders")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Youorder1"))).andExpect(content().contentType(contentType))
				.andDo(document("/orders/all"));

		mockMvc.perform(get("/orders/100")).andExpect(status().isNotFound()).andDo(document("/orders/100"));

		/**
		 * Test create order and generate API document to
		 * target/snippets/orders/create directory.
		 */
		String orderJson = json(mockOrder);

		mockMvc.perform(post("/orders").contentType(contentType).content(orderJson)).andExpect(status().isOk())
				.andDo(document("/orders/create"));
		;

		/**
		 * Test update order and generate API document to
		 * target/snippets/orders/update directory.
		 */
		orderJson = json(mockOrder);

		mockMvc.perform(put("/orders/2").contentType(contentType).content(orderJson)).andExpect(status().isOk())
				.andDo(document("/orders/update"));

	}

	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}
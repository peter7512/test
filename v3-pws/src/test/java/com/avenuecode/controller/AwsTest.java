package com.avenuecode.controller;

import com.avenuecode.domain.Order;
//import org.junit.Assert;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

/**
 * This is the test for remote running on AWS ,It works fine. MayBe you Laptop
 * can not reach Internet now, So I Ignore this way. Automated test for order
 * post and update. *
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AwsTest {
	private static final Logger log = LoggerFactory.getLogger(AwsTest.class);

	@Test
	@Ignore
	public void getProducts() throws Exception {
		/**
		 * test order post ,For simpler, just get a order object by getting and
		 * modify some value of its field and post ,after getting the result
		 * compare the values.
		 */

		RestTemplate restTemplate = new RestTemplate();
		Order order = restTemplate
				.getForObject("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:9090/orders/1", Order.class);
		order.setName("post test");
		order.getOrderProducts().forEach(x -> x.setAmount(99));
		log.info(order.toString());
		Order postedOrder = restTemplate.postForObject(
				"http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:9090/orders", order, Order.class);

		Long id = postedOrder.getOrderId();
		order.setOrderId(id); // new order id should be different.
		order.setOrderedDate(postedOrder.getOrderedDate());
		order.getOrderProducts().forEach(x -> {
			x.setOrderProductId(null);
			x.setOrderId(id);
		}); // setOrderProductId should be different.
		postedOrder.getOrderProducts().forEach(x -> x.setOrderProductId(null));

		Assert.assertEquals(order, postedOrder);

		/**
		 * test for updating order, updating order 1 to order 2 and getting the
		 * result of the order 1.
		 */

		restTemplate.put("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:9000/orders/1", order);
		Order putOrder = restTemplate
				.getForObject("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:9000/orders/1", Order.class);
		order.setOrderId((long) 1);
		order.getOrderProducts().forEach(x -> {
			x.setOrderProductId(null);
			x.setOrderId((long) 1);
		}); // setOrderProductId should be different.
		putOrder.getOrderProducts().forEach(x -> x.setOrderProductId(null));
		Assert.assertEquals(order, putOrder);

	}
}
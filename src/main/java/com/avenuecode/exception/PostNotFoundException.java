package com.avenuecode.exception;

public class PostNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2222222222222222222L;

	public PostNotFoundException(String msg){
		super(msg);
	}
	
}
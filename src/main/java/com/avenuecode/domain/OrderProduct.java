package com.avenuecode.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table(name = "ORDER_PRODUCT")
@Data
public class OrderProduct {
	@Id
	@GeneratedValue
	@Column(name = "ORDER_PRODUCT_ID")
	private Long orderProductId;

	private Long orderId;
	// private Long productId;

	@ManyToOne
	private Product product;

	private Integer amount;

	private String info;

	private OrderProduct() {

	}

	public Long getOrderProductId() {
		return orderProductId;
	}

	public void setOrderProductId(Long orderProductId) {
		this.orderProductId = orderProductId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "OrderProduct [orderProductId=" + orderProductId + ", orderId=" + orderId + ", product=" + product
				+ ", amount=" + amount + ", info=" + info + "]";
	}

}

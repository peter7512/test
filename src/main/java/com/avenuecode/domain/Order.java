package com.avenuecode.domain;

import com.avenuecode.tools.*;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

@Entity
@Table(name = "ORDER1")
@Data
public class Order {
	@Id
	@GeneratedValue
	// @Column(name="ORDER_ID")
	private Long orderId;

	@OneToMany(mappedBy = "orderId")
	List<OrderProduct> orderProducts;

	private String name;
	private String description;

	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderedDate;

	private String information;

	private Order() {
		// orderProducts=new ArrayList<>();
	}

	public Order(String name) {
		this.name = name;
		// orderProducts=new ArrayList<>();
	}

	public List<OrderProduct> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProduct> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getOrderedDate() {
		return orderedDate;
	}
	
	
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderProducts=" + orderProducts + ", name=" + name + ", description="
				+ description + ", orderedDate=" + orderedDate + ", information=" + information + "]";
	}

}

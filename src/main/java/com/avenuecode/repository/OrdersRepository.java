package com.avenuecode.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.domain.Order;

@Repository
public interface OrdersRepository extends CrudRepository<Order, Long>{

}

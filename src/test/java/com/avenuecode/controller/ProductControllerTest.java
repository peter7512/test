package com.avenuecode.controller;

import com.avenuecode.domain.Product;
import com.avenuecode.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test list all products and product by Id.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

	@MockBean
	private ProductService productService;
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getProducts() throws Exception {
		

		List<Product> mockProductList = new ArrayList<>();
		Product mockProduct = new Product("Pen");
		mockProduct.setPrice(9.55);
		mockProduct.setDescription("It is a pen.");
		mockProduct.setInformation("Other info of pen");
		mockProductList.add(mockProduct);

		given(productService.read(1)).willReturn(mockProduct);

		mockProduct = new Product("Paper");
		mockProduct.setPrice(1.55);
		mockProduct.setDescription("It is a paper.");
		mockProduct.setInformation("Other info of paper");
		mockProductList.add(mockProduct);
		given(productService.read(2)).willReturn(mockProduct);

		mockProduct = new Product("Box");
		mockProduct.setPrice(5.55);
		mockProduct.setDescription("It is a box.");
		mockProduct.setInformation("Other info of box");
		mockProductList.add(mockProduct);
		given(productService.read(3)).willReturn(mockProduct);

		given(productService.list()).willReturn(mockProductList);

		this.mockMvc.perform(get("/products")).andExpect(status().isOk())
				.andExpect(content().string(containsString("Box")));

	}
}
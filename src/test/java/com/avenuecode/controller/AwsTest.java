package com.avenuecode.controller;

import com.avenuecode.domain.Order;
//import org.junit.Assert;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

/**
 * This is the test for remote running on AWS ,It works fine. 
 *  Automated test for order post and update. * 
 *  */

@RunWith(SpringJUnit4ClassRunner.class)
public class AwsTest {

	@Test
	public void getProducts() throws Exception {
		/**
		 * test order post ,For simpler, just get a order object by getting and
		 * modify some value of its field and post ,after getting the result
		 * compare the values.
		 */

		RestTemplate restTemplate = new RestTemplate();
		Order order = restTemplate
				.getForObject("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders/2", Order.class);
		order.setName("post test");
		order.getOrderProducts().forEach(x -> x.setAmount(99));
		
		Order postedOrder = restTemplate.postForObject(
				"http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders", order, Order.class);
		
		Long id = postedOrder.getOrderId();
		order.setOrderId(id);   // new order id should be different.
		order.setOrderedDate(postedOrder.getOrderedDate()); 
		order.getOrderProducts().forEach(x -> {
			x.setOrderProductId(null);
			x.setOrderId(id);
		}); // setOrderProductId should be different.
		postedOrder.getOrderProducts().forEach(x -> x.setOrderProductId(null));
        System.out.println(order);
        System.out.println(postedOrder);
		Assert.assertEquals(order.toString(), postedOrder.toString());
	//	Assert.assertEquals(order.getOrderProducts(), postedOrder.getOrderProducts());

		/**
		 * test for updating order, updating order 1 to order 2 and getting the
		 * result of the order 1.
		 */

		restTemplate.put("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders/1", order);
		Order putOrder = restTemplate
				.getForObject("http://ec2-34-208-115-156.us-west-2.compute.amazonaws.com:8080/orders/1", Order.class);
		order.setOrderId((long) 1);
		order.getOrderProducts().forEach(x -> {
			x.setOrderProductId(null);
			x.setOrderId((long) 1);
		}); // setOrderProductId should be different.
		putOrder.getOrderProducts().forEach(x -> x.setOrderProductId(null));
		Assert.assertEquals(order.toString(), putOrder.toString());

	}
}